package config

import (
	"encoding/json"
	"strings"
	"testing"
)

func TestNewConfig(t *testing.T) {
	t.Run("should return a Config with the expected host name", func(t *testing.T) {
		config := config2.config{Host: "notarealhost"}

		fakeFile := strings.NewReader(stringifyJSON(config))
		result := config2.newConfig(fakeFile)

		if result.Host != config.Host {
			t.Error("Unexpected host name")
		}
	})

	t.Run("should panic when a host name is not specified in the Config", func(t *testing.T) {
		config := config2.config{}
		fakeFile := strings.NewReader(stringifyJSON(config))

		defer func() {
			if r := recover(); r == nil {
				t.Errorf("Did not panic as expected")
			}
		}()

		config2.newConfig(fakeFile)
	})
}

func stringifyJSON(config config2.config) string {
	jsonBytes, _ := json.Marshal(config)
	return string(jsonBytes)
}