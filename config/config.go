package config

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"os"
)

type Config struct {
	Host        string `json:"host"`
	Scenario    string `json:"scenario"`
	ArrivalRate int    `json:"arrival_rate"`
	Duration    int    `json:"duration"`
}

func GetTestConfig(configFileName string) Config {
	file, err := os.Open(configFileName)

	if err != nil {
		panic(err.Error())
	}

	config := newConfig(file)
	return config
}

func newConfig(fileReader io.Reader) Config {
	configBytes, err := ioutil.ReadAll(fileReader)

	if err != nil {
		panic(err.Error())
	}

	var config Config

	json.Unmarshal(configBytes, &config)

	if config.Host == "" {
		panic("You need to specify a host in Config.json")
	}

	return config
}
