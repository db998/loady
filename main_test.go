package main

import (
	"bytes"
	"github.com/stretchr/testify/assert"
	"loady/phase"
	"log"
	"os"
	"testing"
)

type mockTestPhase struct{}

func (mtp mockTestPhase) Begin() *[]phase.Result {
	mockResults := []phase.Result{
		{
			Pass: true,
			Fail: false,
		},
		{
			Fail: true,
			Pass: false,
		},
		{
			Pass: true,
			Fail: false,
		},
	}

	return &mockResults
}

func TestBeginTest(t *testing.T) {
	t.Run("should run the phase and output the expected results", func(t *testing.T) {
		var buffer bytes.Buffer
		log.SetOutput(&buffer)

		defer func() {
			log.SetOutput(os.Stderr)
		}()

		log.SetFlags(0)

		BeginTest(mockTestPhase{})

		actualOutput := buffer.String()
		expectedOutput := "2 success, 1 failed\n"

		assert.Equal(t, expectedOutput, actualOutput)
	})
}
