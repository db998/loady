package main

import (
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestSendRequest(t *testing.T) {
	t.Run("should return the HTTP response", func(t *testing.T) {
		server := httptest.NewServer(http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
			writer.WriteHeader(http.StatusOK)
			writer.Write([]byte("hello world"))
		}))

		defer server.Close()

		loady := RequestProcessor{target: server.URL}

		response, error := loady.SendRequest()

		body, _ := ioutil.ReadAll(response.Body)

		actual := string(body)

		assert.Equal(t, actual, "hello world")
		assert.Nil(t, error)
	})
}
