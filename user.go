package main

import "loady/phase"

type User struct {
	Requester RequestProcessor
}

func (u User) Visit() chan phase.Result {
	channel := make(chan phase.Result)

	go func() {
		var result phase.Result
		_, err := u.Requester.SendRequest()

		if err != nil {
			result.Fail = true
		} else {
			result.Pass = true
		}

		channel <- result
		close(channel)
	}()

	return channel
}
