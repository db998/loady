package phase

import (
	"log"
	"sync"
	"time"
)

type User interface {
	Visit() chan Result
}

type FixedArrivalPhase struct {
	User        User
	ArrivalRate int
	TimeLimit   int
}

func (fap *FixedArrivalPhase) Begin() *[]Result {
	var waitGroup sync.WaitGroup

	maxNumberOfRequests := fap.ArrivalRate * (fap.TimeLimit / 1000)

	waitGroup.Add(maxNumberOfRequests)

	var results []Result

	for i := 0; i < maxNumberOfRequests; i += fap.ArrivalRate {
		<-time.After(1 * time.Second)

		fap.sendVirtualUserRequests(&waitGroup, &results)
	}

	log.Println("Waiting for final results")
	waitGroup.Wait()

	return &results
}

func (fap FixedArrivalPhase) sendVirtualUserRequests(waitGroup *sync.WaitGroup, results *[]Result) {
	go func(wg *sync.WaitGroup, results *[]Result) {
		userRequests := *fap.scheduleUserRequests(fap.ArrivalRate)

		for response := range fap.aggregateResults(userRequests) {
			*results = append(*results, response)
			wg.Done()
		}
	}(waitGroup, results)
}

func (fap FixedArrivalPhase) scheduleUserRequests(arrivalRate int) *[]chan Result {
	log.Println("Sending users")
	userGroup := make([]chan Result, 0, arrivalRate)
	user := fap.User

	for i := 0; i < arrivalRate; i++ {
		userGroup = append(userGroup, user.Visit())
	}

	return &userGroup
}

func (fap *FixedArrivalPhase) aggregateResults(resultGroup []chan Result) chan Result {
	resultChannel := make(chan Result)
	var waitGroup sync.WaitGroup

	fanIn := func(response chan Result) {
		for res := range response {
			resultChannel <- res
		}
		waitGroup.Done()
	}

	waitGroup.Add(len(resultGroup))

	for _, result := range resultGroup {
		go fanIn(result)
	}

	go func() {
		waitGroup.Wait()
		close(resultChannel)
	}()

	return resultChannel
}
