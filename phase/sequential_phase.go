package phase

import (
	"net/http"
)

type IRequestProcessor interface {
	SendRequest() (*http.Response, error)
}

type SequentialPhase struct {
	MaxNumberOfRequests int
	Requester           IRequestProcessor
}

func (sp SequentialPhase) Begin() *[]Result {
	results := make([]Result, 0, sp.MaxNumberOfRequests)

	for i := 0; i < sp.MaxNumberOfRequests; i++ {
		_, err := sp.Requester.SendRequest()

		result := Result{}

		if err != nil {
			result.Fail = true
		} else {
			result.Pass = true
		}

		results = append(results, result)
	}

	return &results
}
