package phase

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"net/http"
	"testing"
)

type mockRequestProcessor struct {
	mock.Mock
}

func (m mockRequestProcessor) SendRequest() (*http.Response, error) {
	args := m.Called()
	return nil, args.Error(1)
}

func TestSequentialPhase_Begin(t *testing.T) {
	t.Run("should return the expected pass/fail count", func(t *testing.T) {
		requester := &mockRequestProcessor{}
		requester.On("SendRequest", mock.Anything).Return(nil, errors.New("mock http error")).Once()
		requester.On("SendRequest", mock.Anything).Return(nil, nil).Once()
		requester.On("SendRequest", mock.Anything).Return(nil, errors.New("mock http error3")).Once()
		phase := &SequentialPhase{3, *requester}

		results := phase.Begin()

		success := 0
		failures := 0

		for _, result := range *results {
			if result.Pass {
				success++
			} else {
				failures++
			}
		}

		assert.Equal(t, len(*results), 3)
		assert.Equal(t, success, 1)
		assert.Equal(t, failures, 2)
	})
}
