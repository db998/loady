package main

import (
	"fmt"
	"loady/config"
	"loady/phase"
	"log"
	"os"
)

type TestPhase interface {
	Begin() *[]phase.Result
}

func main() {
	validateInputParams()
	log.Println("Starting test")

	configFileName := os.Args[1]
	testConfig := config.GetTestConfig(configFileName)

	requester := RequestProcessor{target: testConfig.Host}
	user := User{Requester: requester}

	switch testConfig.Scenario {
	case "sequential":
		testPhase := phase.SequentialPhase{testConfig.ArrivalRate, requester}
		BeginTest(testPhase)
	case "constant_arrival":
		testPhase := phase.FixedArrivalPhase{User: user, TimeLimit: testConfig.Duration, ArrivalRate: testConfig.ArrivalRate}
		BeginTest(&testPhase)
	}
}

func validateInputParams() {
	if len(os.Args) < 2 {
		panic("You must specify the config path as the first argument")
	}
}

func BeginTest(phase TestPhase) {
	results := phase.Begin()

	success, failure := aggregateResults(results)
	showResults(success, failure)
}

func showResults(success int, failure int) {
	output := fmt.Sprintf("%d success, %d failed", success, failure)

	log.Println(output)
}

func aggregateResults(results *[]phase.Result) (int, int) {
	successCount := 0
	failureCount := 0

	for _, result := range *results {
		if result.Pass {
			successCount++
		} else {
			failureCount++
		}
	}

	return successCount, failureCount
}
