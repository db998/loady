package main

import "net/http"

type RequestProcessor struct {
	target string
}

func (rp RequestProcessor) SendRequest() (*http.Response, error) {
	request, _ := http.NewRequest(http.MethodGet, rp.target, nil)
	client := http.Client{}

	return client.Do(request)
}
