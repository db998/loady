# loady :rocket:
Load testing tool

## Config
```json
{
  "host": "http://localhost:3000",
  "scenario": "constant_arrival",
  "arrival_rate": 50,
  "duration": 10000
}
```
This will send an HTTP request to `http://somehost.com` ~ 3 times a second

## Running the tool
```bash
$ go run !(*_test).go <file_name>
```

## Running the tests
```bash
$ go test ./ -v
```

## Build
```bash
$ go build -v
```